package lt.jrd.javakisapp.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import lt.jrd.javakisapp.Exceptions.AuthException;
import lt.jrd.javakisapp.Models.User;
import lt.jrd.javakisapp.R;
import lt.jrd.javakisapp.SendEmailActivity;
import lt.jrd.javakisapp.Utils.ValueKeys;

public class RegisterFragment extends Fragment {


    @InjectView(R.id.username) EditText username;
    @InjectView(R.id.password1) EditText password1;
    @InjectView(R.id.password2) EditText password2;

    public static RegisterFragment newInstance() {
        RegisterFragment fragment = new RegisterFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.inject(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }


    @OnClick(R.id.register)
    void register(Button btn) {
        try {
            boolean status = User.register(username.getText().toString(), password1.getText().toString(), password2.getText().toString(), getActivity());
            Intent i = new Intent(getActivity(), SendEmailActivity.class);
            startActivity(i);
            ValueKeys.setLoggedUserId(User.getByEmail(username.getText().toString()).getId());
            getActivity().finish();
        } catch (AuthException e) {
            Toast toast = Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
