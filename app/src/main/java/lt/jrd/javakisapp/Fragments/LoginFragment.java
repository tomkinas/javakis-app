package lt.jrd.javakisapp.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import lt.jrd.javakisapp.Models.User;
import lt.jrd.javakisapp.R;
import lt.jrd.javakisapp.SendEmailActivity;
import lt.jrd.javakisapp.Utils.ValueKeys;

public class LoginFragment extends Fragment {

    @InjectView(R.id.username) EditText username;
    @InjectView(R.id.password) EditText password;


    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.inject(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @OnClick(R.id.login)
    void login(Button loginButton) {
        User loggedInUser = User.login(username.getText().toString(), password.getText().toString());
        if (loggedInUser != null) {
            SendEmailActivity.OPEN(getActivity());
            ValueKeys.setLoggedUserId(loggedInUser.getId());
            getActivity().finish();
        } else {
            Toast toast = Toast.makeText(getActivity(), R.string.invalid_login, Toast.LENGTH_LONG);
            toast.show();
        }
    }
}
