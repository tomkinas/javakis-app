package lt.jrd.javakisapp.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.InjectViews;
import butterknife.OnClick;
import lt.jrd.javakisapp.R;
import lt.jrd.javakisapp.Utils.ValueKeys;

public class SendEmailActivityFragment extends Fragment {

    @InjectView(R.id.text_form) EditText form;
    @InjectView(R.id.contact_info) EditText contactInfo;
    @InjectViews({R.id.email_button, R.id.skype_button, R.id.phone_button}) List<ImageView> actionButton;


    private String contactType;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_send_email, container, false);
        ButterKnife.inject(this, view);

        actionButton.get(0).performClick();
        return view;
    }

    @OnClick({R.id.email_button, R.id.skype_button, R.id.phone_button})
    void clickPreferedContact(ImageView view) {
        ButterKnife.apply(actionButton, DESELECT);
        view.setSelected(true);

        switch (view.getId()) {
            case R.id.phone_button:
                contactInfo.setHint(R.string.enter_your_phone_number);
                contactInfo.setText("");
                contactType = getString(R.string.via_phone);
                break;
            case R.id.skype_button:
                contactInfo.setHint(R.string.enter_your_username);
                contactInfo.setText("");
                contactType = getString(R.string.via_skype);
                break;
            case R.id.email_button:
                contactInfo.setHint(R.string.enter_your_email);
                contactInfo.setText(ValueKeys.getCurrentUser().getEmail());
                contactType = getString(R.string.via_email);
                break;
        }
    }

    @OnClick(R.id.send_email)
    void sendEmail() {
        sendEmailIntent("klausk@eurodesk.lt", "naujas klausimas", form.getText() + "\n\nAtsakymą pageidauju gauti " + contactType + ": " + contactInfo.getText());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    public void sendEmailIntent(String to, String subject, String text) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", to, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, text);
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    static final ButterKnife.Action<View> DESELECT = new ButterKnife.Action<View>() {
        @Override
        public void apply(View view, int index) {
            view.setSelected(false);
        }
    };
}
