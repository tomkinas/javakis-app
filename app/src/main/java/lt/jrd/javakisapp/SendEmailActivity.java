package lt.jrd.javakisapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import lt.jrd.javakisapp.Utils.ValueKeys;


public class SendEmailActivity extends AppCompatActivity {

    public static void OPEN(Context context) {
        Intent i = new Intent(context, SendEmailActivity.class);
        context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_email);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_send_email, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.logout) {
            ValueKeys.setLoggedUserId(null);
            LoginActivity.OPEN(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
