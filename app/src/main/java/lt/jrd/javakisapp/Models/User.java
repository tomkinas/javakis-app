package lt.jrd.javakisapp.Models;

import android.content.Context;
import android.text.TextUtils;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import lt.jrd.javakisapp.Exceptions.AuthException;
import lt.jrd.javakisapp.R;

@Table(name = "Users")
public class User extends Model {

    @Column(name = "email", unique = true)
    String email;

    @Column(name = "password")
    String password;

    public User() {
        super();
    }

    public User(String email, String password) {
        super();
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }


    public static User getByEmail(String username) {
        return new Select()
                .from(User.class)
                .where("email like ?", username)
                .executeSingle();
    }

    public static User getById(Long id) {
        return new Select()
                .from(User.class)
                .where("id = ?", id)
                .executeSingle();
    }

    public static boolean register(String email, String password, String passRepeat, Context context) throws AuthException {
        if (password.equals(passRepeat)) {
            return register(email, password, context);
        } else {
            throw new AuthException(R.string.pasword_does_now_match, context);
        }
    }

    public static boolean register(String email, String password, Context context) throws AuthException {
        User user;

        user = User.getByEmail(email);
        if (user != null) {
            throw new AuthException(R.string.username_taken, context);
        }

        if (!isValidEmail(email)) {
            throw new AuthException(R.string.invalid_email, context);
        }

        if (!isValidPassword(password)) {
            throw new AuthException(R.string.too_short_password, context);
        }

        User u = new User(email, password);
        long status = u.save();

        if (status >= 0) {
            return true;
        } else {
            throw new AuthException(R.string.unknown_error, context);
        }
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isValidPassword(CharSequence target) {
        return target.length() > 5;
    }


    public static User login(String username, String password) {
        User u = User.getByEmail(username);
        if (u == null) {
            return null;
        }

        if (u.password.equals(password)) {
            return u;
        }

        return null;
    }

}
