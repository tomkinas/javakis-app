package lt.jrd.javakisapp;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;
import com.orhanobut.hawk.Hawk;

import io.fabric.sdk.android.Fabric;
import lt.jrd.javakisapp.Utils.ValueKeys;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        Hawk.init(this);

        if (isUserLoggedIn()) {
            SendEmailActivity.OPEN(this);
            finish();
        } else {
            LoginActivity.OPEN(this);
            finish();
        }
    }

    public boolean isUserLoggedIn() {
        return ValueKeys.getLoggedUserId() != null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
