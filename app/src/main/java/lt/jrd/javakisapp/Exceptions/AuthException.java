package lt.jrd.javakisapp.Exceptions;

import android.content.Context;

public class AuthException extends Exception {

    public AuthException(int message, Context context) {
        this(context.getString(message));
    }
    
    public AuthException(String message) {
        super(message);
    }
}

