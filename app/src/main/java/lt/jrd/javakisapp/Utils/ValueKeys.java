package lt.jrd.javakisapp.Utils;

import com.orhanobut.hawk.Hawk;

import lt.jrd.javakisapp.Models.User;

public class ValueKeys {

    private static String loggedUserId = "LOGGED_USER_ID";

    public static Long getLoggedUserId() {
        return Hawk.get(loggedUserId, null);
    }

    public static void setLoggedUserId(Long id) {
        Hawk.put(loggedUserId, id);
    }

    public static User getCurrentUser() {
        return User.getById(getLoggedUserId());
    }
}
